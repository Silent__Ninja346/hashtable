#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "hash_table.h"
#include "prime.h"

#define len(x) (sizeof(x) / sizeof(*x))

void is_prime_test() {
  const int prime_nums[] = {2, 3, 5, 7, 11, 13, 53, 283, 1999, 2797};
  for (int i = 0; i < len(prime_nums); i++)
    assert(is_prime(prime_nums[i]) == 1);

  printf("check for primes passed\n");

  const int non_prime_nums[] = {4, 6, 12, 1100, 150, 77, 69696969};
  for (int i = 0; i < len(non_prime_nums); i++)
    assert(is_prime(non_prime_nums[i]) == 0);

  printf("check for non-primes passed\n");

  const int undefined_nums[] = {-55, -3, 0, 1};
  for (int i = 0; i < len(undefined_nums); i++)
    assert(is_prime(undefined_nums[i]) == -1);

  printf("check for undefined (x < 2) passed\n");
}

void next_prime_test() {
  const int not_primes[] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
  const int prime_nums[] = {2, 3, 5, 5, 7, 7, 11, 11, 11, 11, 13};

  assert(len(not_primes) == len(prime_nums));

  for (int i = 0; i < len(not_primes); i++) {
    assert(next_prime(not_primes[i]) == prime_nums[i]);
  }
  printf("check for finding next prime number passed\n");
}

void ht_new_test() {
  ht_hash_table *ht = ht_new();

  assert(ht->base_size == 500);
  assert(ht->count == 0);
  assert(ht->size == next_prime(ht->base_size));

  for (int i = 0; i < ht->size; i++) {
    assert(ht->items[i] == NULL);
  }

  ht_del_hash_table(ht);

  printf("check for creation of new hash table passed\n");
}

void ht_insert_search_test() {
  ht_hash_table *ht = ht_new();
  ht_insert(ht, "hello", "world");
  ht_insert(ht, "thae", "herann");
  ht_insert(ht, "cal", "kestis");
  assert(ht->count == 3);

  assert(strcmp(ht_search(ht, "hello"), "world") == 0);
  assert(strcmp(ht_search(ht, "thae"), "herann") == 0);
  assert(strcmp(ht_search(ht, "cal"), "kestis") == 0);

  ht_insert(ht, "hello", "WORLD");
  assert(strcmp(ht_search(ht, "hello"), "WORLD") == 0);

  assert(ht_search(ht, "I don't exist") == NULL);

  ht_del_hash_table(ht);

  printf("check for insert and search passed\n");
}

void ht_delete_test() {
  ht_hash_table *ht = ht_new();
  ht_insert(ht, "hello", "world");
  ht_insert(ht, "thae", "herann");
  assert(ht->count == 2);

  ht_delete(ht, "hello");
  assert(ht->count == 1);
  assert(ht_search(ht, "hello") == NULL);
  assert(strcmp(ht_search(ht, "thae"), "herann") == 0);

  ht_delete(ht, "thae");
  assert(ht->count == 0);
  assert(ht_search(ht, "thae") == NULL);

  ht_del_hash_table(ht);

  printf("check for item deletion passed\n");
}

int main() {
  is_prime_test();
  next_prime_test();
  ht_new_test();
  ht_insert_search_test();
  ht_delete_test();
  return 0;
}
